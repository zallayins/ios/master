//
//  viewElements.swift
//  PCMaster
//
//  Created by Hugo Santiago on 13/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

extension Array {
    func split() -> [[Element]] {
        let ct = self.count
        let half = ct / 2
        let leftSplit = self[0 ..< half]
        let rightSplit = self[half ..< ct]
        return [Array(leftSplit), Array(rightSplit)]
    }
}

struct viewElements: View {
    
    var cosas: [CarritoItemcito]
    
    var body: some View {
        ScrollView (.horizontal){
           HStack {
                ForEach(cosas.split()[0]){ item in
                    element(image: item.productoImagen, name: item.productoNombre, price: Int(item.precio), product: item)
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                }
            }
            HStack{
                ForEach(cosas.split()[1]){ item in
                    element(image: item.productoImagen, name: item.productoNombre, price: Int(item.precio), product: item)
                        .padding(.bottom, 30)
                        .padding(.horizontal, 20)
                }
            }
        }
        .edgesIgnoringSafeArea(.horizontal)
    }
}

struct element: View {
    @EnvironmentObject var carrito:CarritoController
    
    var image: String
    var name: String
    var price: Int
    var product: CarritoItemcito
    
    var body: some View {
        ZStack(alignment: .top){
            VStack (alignment: .leading, spacing: 16){
                AsyncImage(url: URL(string: image)! , placeholder: Image(systemName: "goforward").frame(width: 150, height: 200))
                VStack(alignment: .leading, spacing: 5){
                    Text("\(name)")
                        .foregroundColor(.primary)
                        .font(.system(size:20))
                        .fontWeight(.black)
                    HStack {
                        Text("\(price) usd")
                            .foregroundColor(.secondary)
                            .font(.caption)
                        Spacer()
                        Text("\(product.cantidad)")
                            .foregroundColor(.secondary)
                            .font(.caption)
                    }
                }
                .padding(.horizontal)
            }
            .padding(.vertical)
            .frame(width: 150, height: 200)
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(20)
            .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
            
            HStack {
                Spacer()
                Button(action: {
                    withAnimation{
                        self.carrito.deleteItem(self.product)
                    }
                }) {
                Image(systemName: "cart.badge.minus")
                    .resizable()
                    .frame(width:20,height: 20)
                    .foregroundColor(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                    .padding()
                    .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
                }
            }
        }
        
    }
}
