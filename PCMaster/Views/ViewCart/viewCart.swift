//
//  viewCart.swift
//  PCMaster
//
//  Created by Hugo Santiago on 13/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI
import Combine

struct viewCart: View {
    
    @EnvironmentObject var carrito:CarritoController
    var body: some View {
        
        VStack{
             viewElements(cosas: carrito.carrito)
            
            Divider()
            
            HStack(spacing: 100){
                VStack(alignment: .leading, spacing: 15){
                    Text("Subtotal")
                        .font(.system(size:20))
                    Text("IVA")
                        .font(.system(size:20))
                    Text("Total")
                        .font(.system(size:30, weight: .heavy))
                }
                
                VStack(alignment: .trailing, spacing: 15){
                    Text("\(carrito.calcularTotal())")
                        .font(.system(size:20))
                    Text("\(carrito.calcularTotalIVA())")
                        .font(.system(size:20))
                    Text("\(carrito.calcularTotalIVA() + carrito.calcularTotal())")
                        .font(.system(size:30, weight: .heavy))
                }
            }
            .padding()
            
            Spacer()
            Button(action: {
                self.carrito.pagar()
            }) {
                Text("Pagar")
                    .frame(minWidth:0, maxWidth: 250)
                    .frame(height:50)
                    .foregroundColor(.white)
                    .font(.system(size: 18, weight: .bold))
                    .background(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                    .cornerRadius(100)
            }
        }
        .navigationBarTitle("Carrito")
    }
}

