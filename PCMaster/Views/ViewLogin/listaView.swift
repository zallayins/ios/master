//
//  listaView.swift
//  PCMaster
//
//  Created by Hugo Santiago on 27/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct listaView: View {
    var compritas: [compras]
    @State var paso: Bool = false
    @State var gg:[items] = []
    var body: some View {
        List(compritas){cosa in
            
            HStack{
                AsyncImage(url: URL(string: cosa.itemsitos[0].imagen)!, placeholder: Image(systemName: "goforward"))
                    .frame(width:70, height:70)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 4))
                .onTapGesture {
                    self.paso.toggle()
                    self.gg = cosa.itemsitos
                }
                VStack{
                    Text("Cantidad: \(Int(cosa.itemsitos.count))")
                    Text("Total: \(Int(cosa.total)) USD")
                }
            }
            .sheet(isPresented: self.$paso) {
                detailLista(lista: self.gg)
            }
        }
    }
}

struct detailLista: View{
    var lista: [items]
    var body: some View{
        ForEach(lista){p in
            ScrollView(.vertical){
                VStack{
                    listaItem(componente: p)
                }
            }
        }
    }
}

struct listaItem: View {
    
    var componente: items
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            ZStack(alignment: .bottom) {
                AsyncImage(url: URL(string: componente.imagen)!, placeholder: Image(systemName: "goforward"))
                    .frame(width: 300, height: 300 )
                    .cornerRadius(10)
                ButtonComponetItem(componente:componente)
                
            }
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(15)
            .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
        }
    }
}

private struct ButtonComponetItem: View {
    var componente: items
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(componente.productName)
                .foregroundColor(.primary)
                .font(.headline)
            Text("Cantidad: \(componente.cantidad.description)")
                .padding(.leading,1)
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
            Text("Precio: \(Int(componente.precio)) USD")
                .padding(.leading,1)
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
            Text("Tienda: \(componente.tiendaName)")
                .padding(.leading,1)
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
        }
        .frame(width: 300, height: 300*0.3)
        .background(Color.white)
        .cornerRadius(10, corners: [.bottomLeft, .bottomRight])
        .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
    }
}
