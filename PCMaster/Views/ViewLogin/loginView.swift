//
//  loginView.swift
//  PCMaster
//
//  Created by Hugo Santiago on 27/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct CheckView: View {
    @State var isChecked:Bool = false
    var title:String = "Recordar"
    
    func toggle(){isChecked = !isChecked}
    
    var body: some View {
        Button(action: toggle){
            HStack{
                Image(systemName: isChecked ? "checkmark.square": "square")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 25)
                    .foregroundColor(.black)
                Text(title)
                    .font(.system(size: 15))
                    .foregroundColor(.black)
            }
        }
    }
}

struct signInView: View {
    @State var email : String = ""
    @State var password : String = ""
    @State var error: String = ""
    
    @EnvironmentObject var session: SessionStore
    
    @State var isPressed: Bool = false
    
    let defaults = UserDefaults.standard
    
    func signIn(){
        session.signIn(email: email, password: password){ (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            }else{
                guard let user = result?.user else { return }
                let isAnonymous = user.isAnonymous  // true
                let uid = user.uid
                let defaults = UserDefaults.standard;
                defaults.set(uid, forKey: "uid")
                defaults.set(isAnonymous, forKey: "isAnonymous")
                defaults.set(self.email, forKey: "login")
                self.email = ""
                self.password = ""
                if let ss = defaults.string(forKey: "uid")
                {
                    print("UID: \(ss)");
                }
                if let ss = defaults.string(forKey: "isAnonymous")
                {
                    print("isAnonymous: \(ss)");
                }
                if let ss = defaults.string(forKey: "login")
                {
                    print("login: \(ss)");
                }
            }
            
        }
    }
    
    var body: some View{
        VStack{
            Image("iconoTemp")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: 350)
                .offset(y:-70)
            VStack {
                HStack {
                    Text("Login")
                        .font(.system(size: 22 , weight: .heavy))
                        .padding(.horizontal)
                    Spacer()
                    HStack{
                        Text ("Crear cuenta")
                            .font(.system(size:18, weight: .semibold))
                            .foregroundColor(Color(.blue))
                            .padding(.horizontal)
                            .onTapGesture(perform:{ self.isPressed.toggle() })
                    }.sheet(isPresented: $isPressed, content: {signUpView()})
                        .padding()
                    
                }
                VStack{
                    TextField("Email ", text: $email)
                        .font(.system(size:14))
                        .padding(12)
                        
                        .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.black), lineWidth:1 ))
                    
                    SecureField("Contraseña", text: $password)
                        .font(.system(size:14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.black), lineWidth:1 ))
                    
                    Text("")
                    
                }
                .padding(.horizontal)
            }
            .padding(.vertical)
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(20)
            .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
            .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
            .offset(y:-150)
            
            VStack {
                CheckView()
                    .padding(.trailing)
                
                Button(action: {
                    self.signIn()
                }){
                    Text("Ingresar")
                        .frame(minWidth:0, maxWidth: .infinity)
                        .frame(height:50)
                        .foregroundColor(.white)
                        .font(.system(size: 18, weight: .bold))
                        .background(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                        .cornerRadius(5)
                }
                .background(Color(.sRGB, red: 233, green: 235, blue: 239))
                .cornerRadius(15)
                .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
                .padding(.top, 30)
            }
            .offset(y:-120)

            if (error != ""){
                Text(error)
                    .font(.system(size:14, weight:.semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            Spacer()
        }
        .padding(.horizontal, 32)
    }
}

struct signUpView : View{
    
    @ObservedObject private var suc = SignUpController()
    
    @State var email = ""
    @State var password = ""
    
    @State var error = ""
    @EnvironmentObject var session : SessionStore
    func signUp(){
        session.signUp(email: self.email, password: self.password){ (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            }else{
                self.email = ""
                self.password = ""
            }
        }
    }
    var body: some View {
        VStack {
            Image("iconoTemp")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: 350)
                .offset(y:-70)
            //Stack para la vista del card de email y contraseña
            VStack {
                HStack {
                    Text("Crea tu cuenta")
                        .font(.system(size: 22 , weight: .heavy))
                        .padding(.horizontal)
                }
                VStack{
                    TextField("Email ", text: $suc.user)
                        .font(.system(size:14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.black), lineWidth:1 ))
                    
                    Text(suc.usernameMessage)
                        .foregroundColor(.red)
                        .font(.subheadline)
                    
                    SecureField("Contraseña", text: $suc.password)
                        .font(.system(size:14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.black), lineWidth:1 ))
                    
                    SecureField("Repite la contraseña", text: $suc.passAgain)
                        .font(.system(size:14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.black), lineWidth:1 ))
                    
                    Text(suc.passwordMessage)
                        .foregroundColor(.red)
                        .font(.subheadline)
                    
                    Text("")
                  
                    Button(action: {
                        self.email = self.suc.user
                        self.password = self.suc.password
                        self.signUp()
                    }){
                        Text("Crear")
                            .frame(minWidth:0, maxWidth: .infinity)
                            .frame(height: 50)
                            .foregroundColor(Color(.white))
                            .font(.system(size:17, weight: .bold))
                            .background(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                    }.disabled(!suc.isValid)
                    .cornerRadius(15)
                    
                }
                .padding(.horizontal)
                
            }
            .padding(.vertical)
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(20)
            .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
            .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
            .offset(y:-150)
            
            
            if (error != ""){
                Text(error)
                    .font(.system(size:14, weight:.semibold))
                    .foregroundColor(.red)
                    .padding()
                    .offset(y:-80)
            }
            Spacer()
        }
        .padding(.horizontal,32)
    }
}
let bounds: CGRect = UIScreen.main.bounds
var isSmall:Bool {
    get{
        return (bounds.height == 667 && bounds.width == 375)
    }
}

struct loginView: View {
    var body: some View {
        signInView()
    }
}

