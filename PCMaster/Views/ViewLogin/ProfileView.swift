//
//  ProfileView.swift
//  PCMaster
//
//  Created by Hugo Santiago on 4/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    
    @State var isPressed: Bool = false
    @State var isPressedCompras: Bool = false
    
    @ObservedObject var users = UserController(idUser: SimpleDataManager.getString(key: "uid")!)
    
    @EnvironmentObject var session: SessionStore
    let defaults = UserDefaults.standard
    
    
    var body: some View {
        VStack {
            ZStack{
                Image("iconoTemp")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width:280)
                    .offset(x:-100,y:-350)
                    .position(x:200,y:420)
                
                VStack {
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                    VStack {
                        Text(users.user.nombre)
                            .font(.system(size: 32))
                            .foregroundColor(.black)
                        Text(users.user.correo).font(.subheadline)
                            .foregroundColor(.black)
                    }
                    .frame(maxWidth: .infinity)
                    MenuRow(forIcon: "person.circle", withLabel: "Cerrar sesión").onTapGesture {
                        self.defaults.set(nil, forKey: "uid")
                        self.session.signOut()
                    }
                    HStack {
                        Button(action: {
                            if self.isPressedCompras == false { self.isPressedCompras = true }
                            if self.isPressed == true { self.isPressed = false }
                        }) {
                            
                            VStack {
                                Text("Compras")
                                    .font(.system(size: 20))
                                    .frame(width: 120, alignment: .center)
                                    .foregroundColor(.black)
                                Text(users.user.compras.count.description)
                                    .foregroundColor(.black)
                            }
                            
                        }
                    }
                    .padding()
                }
                    
                .frame(height:  250)
                .padding(.vertical)
                .background(Color(.sRGB, red: 233, green: 235, blue: 239))
                .cornerRadius(20)
                .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
                .padding(.horizontal)
                .overlay(ProfileImage().offset(x: 0, y: -150))
                .position(x:200,y:320)
            }
                        
            if isPressedCompras{
                funcion(cosas: users.user.compras, isPressed: isPressedCompras, name: "Compras")
                    .animation(.linear)
            }
        }
    }
}

private struct ButtonComponetItem: View {
    
    var componente : compras
    
    func fecha(fecha: String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss "
        guard let date = formatter.date(from: fecha) else {
            return ""
        }
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        formatter.dateFormat = "dd"
        let day = formatter.string(from: date)
        
        return year+month+day
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(fecha(fecha: componente.fecha.dateValue().description))
                .foregroundColor(.primary)
                .font(.headline)
            Text("Total: \(Int(componente.total)) USD")
                .padding(.leading,1)
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
        }
        .frame(width: 210, height: 210*0.3)
        .background(Color.white)
        .cornerRadius(10, corners: [.bottomLeft, .bottomRight])
    }
}

struct funcion: View {
    
    var cosas: [compras]
    @State var isPressed: Bool
    @State var vermas: Bool = false
    var name: String
    
    var body: some View{
        VStack(alignment: .leading){
            if isPressed {
                HStack{
                    Text(name)
                        .font(.system(size: 35, weight: .bold))
                        .multilineTextAlignment(.leading)
                        .padding()
                    Spacer()
                    HStack{
                    Text("ver más")
                        .padding()
                        .onTapGesture { self.vermas.toggle()}
                    }
                    .sheet(isPresented: self.$vermas) {
                        listaView(compritas: self.cosas)
                    }
                    
                }
                
                ScrollView(.horizontal, showsIndicators: true){
                    
                    HStack{
                        ForEach(cosas){itemA in
                            VStack(alignment: .leading, spacing: 16) {
                                ZStack(alignment: .bottom) {
                                    
                                    AsyncImage(url: (URL(string: itemA.itemsitos[0].imagen)!), placeholder: Image(systemName: "goforward"))
                                        .frame(width: 210, height: 210 )
                                        .cornerRadius(10)
                                    
                                    ButtonComponetItem(componente: itemA)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        .background(Color(.sRGB, red: 233, green: 235, blue: 239))
        .cornerRadius(15)
        .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
        .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
        .padding(.vertical)
        
    }
}

struct MenuRow: View {
    var label: String
    var icon: String
    init(forIcon icon: String, withLabel label: String) {
        self.label = label
        self.icon = icon
    }
    var body: some View {
        HStack {
            Image(systemName: self.icon)
                .font(.system(size: 20, weight: .regular))
                .imageScale(.large)
                .frame(width: 32, height: 32)
                .foregroundColor(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
            
            Text(self.label)
                .font(.system(size: 20))
                .frame(width: 120, alignment: .leading)
                .foregroundColor(.black)
        }
        .frame(alignment : .center)
    }
}

struct ProfileImage: View {
    var body: some View {
        Image("william")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 120, height: 120, alignment: .center)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 4))
            .shadow(radius: 10)
    }
}

