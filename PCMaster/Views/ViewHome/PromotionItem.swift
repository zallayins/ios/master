//
//  PromotionItem.swift
//  PCMaster
//
//  Created by William Duarte on 27/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct PromotionItem: View {
    
    var promocion: Promocion
    
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            
            ZStack(alignment: .bottom) {
                AsyncImage(
                    url: URL(string: promocion.imagen)!, placeholder: Image(systemName: "goforward")
                )
                .frame(width: 330, height: 200)
                .cornerRadius(10)
                ButtonPromotionItem(promocion: promocion)
            }
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(20)
            .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
        }
    }
}

private struct ButtonPromotionItem: View {
    var promocion: Promocion
    
    var body: some View {
        HStack(alignment: .center, spacing: 30) {
            VStack {
                Text(promocion.producto)
                    .foregroundColor(.primary)
                    .font(.headline)
                Text(promocion.tienda)
                    .foregroundColor(.secondary)
                    .font(.headline)
            }
            VStack {
                if (promocion.descuento == 0) {
                    Text("Precio $\(promocion.precio)")
                        .foregroundColor(.primary)
                        .font(.headline)
                } else {
                    Text("Antes $\(Int(promocion.precio))")
                        .foregroundColor(.red)
                        .font(.subheadline)
                        .strikethrough()
                    Text("Ahora $\(Int(Double(promocion.precio)*(1.0-promocion.descuento)))")
                        .font(.subheadline)
                        .foregroundColor(.primary)
                }
            }
            
        }
        .frame(width: 330, height: 200*0.3)
        .background(Color.white)
        .cornerRadius(10, corners: [.bottomLeft, .bottomRight])
    }
}
