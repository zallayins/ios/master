//
//  MenuCategorias.swift
//  PCMaster
//
//  Created by William Duarte on 26/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//
import SwiftUI

struct MenuCategorias: View {
    
    var lstProducto: [Producto]
    
    var body: some View {
        VStack(alignment: .leading) {
            ScrollView (.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(lstCategoriasTest) { categoria in
                        NavigationLink(destination: ViewCategoria(productos: self.lstProducto, nameCategory: categoria.name)){
                            VStack{
                                ImgMenuIcon(img: Image(categoria.sysImage!))
                                Text(categoria.name)
                                    .font(.system(size: 10))
                            }
                            .padding(.top, 3)
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
}

private struct ImgMenuIcon:View {
    var img: Image
    var body: some View {
        img
            .resizable()
            .aspectRatio(contentMode: .fill)
            .padding()
            .clipShape(Circle())
            .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 3))
            .frame(width:70,height: 70)
            .padding(.horizontal, 5)
    }
}

