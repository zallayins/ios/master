//
//  ComponenteItem.swift
//  PCMaster
//
//  Created by William Duarte on 17/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct ComponenteItem: View {
    
    var componente: Producto
    
    
    var body: some View {
        HStack( spacing: 12) {
            AsyncImage(url: URL(string: componente.imagen)!, placeholder: Image(systemName: "goforward"))
                .frame(width: 150, height: 150)
            ButtonComponetItem(componente:componente)
        }
        .background(Color(.sRGB, red: 233, green: 235, blue: 239))
        .cornerRadius(15)
        .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
        
    }
}

private struct ButtonComponetItem: View {
    var componente: Producto
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(componente.nombre)
                .foregroundColor(.primary)
                .lineLimit(2)
                .font(.headline)
            Text(componente.categoria)
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
            Text("Disponible en \(componente.tiendas.count) tiendas")
                .font(.subheadline)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(height: 40)
            
        }
        .frame(width: UIScreen.screenWidth-160)
        .background(Color.white)
    }
}
