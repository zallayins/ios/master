//
//  PromotionRow.swift
//  PCMaster
//
//  Created by William Duarte on 27/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct PromotionRow: View {
    
    var lstPromociones:[Promocion]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Promociones")
                .font(.title)
                .padding(.leading, 15)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top) {
                    ForEach(self.lstPromociones) { promocion in
                        PromotionItem(promocion: promocion)
                            .frame(width: 330)
                            .padding(10)
                    }
                }
            }
            .offset(y:-25)
        }
    }
}

