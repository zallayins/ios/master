//
//  ComponenteRow.swift
//  PCMaster
//
//  Created by William Duarte on 17/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}

struct ComponenteRow: View {
    
    var lstComponentes: [Producto]
    
    var body: some View {
        VStack{
            ScrollView(.vertical){
                ForEach(lstComponentes){componente in
                    NavigationLink(destination: ComponentDetail(component: componente)){
                        ComponenteItem(componente: componente)
                            .padding(.top,10)
                    }
                }
            }
        }
    }
}
