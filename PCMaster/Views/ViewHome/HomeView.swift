//
//  HomeView.swift
//  PCMaster
//
//  Created by William Duarte on 17/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI
import Combine


struct HomeView: View {
    
    @State var busqueda: String = ""
    @State var isDone = false
    
    //---------------------------------
    @EnvironmentObject var session: SessionStore
    
    //---------------------------------
    @State var isPerform: Bool = false
    @State var isPressed:Bool = true
    
    @State var user: Bool = false
    @State var carrito:Bool = false
    //---------------------------------
    @State var promos:[Promocion]
    @State var productos:[Producto]
    @State var categorias:[Categoria]
    @State var usuarios:[Usuario]

    //---------------------------------
    var controllerProductos = ProductsController()
    var controllerProm = PromController()
    var controllerCateg = CategController()
    //---------------------------------
    func cargar(){
        
        
        if !isDone{
            print("cargando...")
            controllerCateg.getCategsName(callback: { (data:[Categoria]) in
                self.categorias = data
            })
            
            controllerProductos.getProductsName(callback: { (data:[Producto]) in
                self.productos = data
            })
            
            controllerProm.getPromsName(callback: { (data:[Promocion]) in
                self.promos = data
            })
        }
        if promos.count > 0 && productos.count > 0 && categorias.count > 0{
            isDone = true
        }
    }
    
    //---------------------------------
    
    let defaults = UserDefaults.standard
    func getUser(){
        session.listen()
    }
    
    var body: some View {
        NavigationView {
            ZStack (alignment: .bottom) {
                VStack {
                    VStack (alignment: .center, spacing: 0) {
                        HStack (alignment: .center){
                            
                            HStack (alignment: .center, spacing: 0) {
                                Image(systemName: "magnifyingglass")
                                    .resizable()
                                    .frame(width:22,height: 22)
                                    .foregroundColor(Color.gray)
                                    .padding(0)
                                TextField("Search", text: $busqueda)
                                    .padding(10)
                                    .frame(height: 40)
                            }
                            .frame(height: 25)
                            .padding(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.gray, lineWidth: 1)
                            )
                            //------------------------
                            NavigationLink(destination: viewCart()) {
                                Image(systemName: "cart")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width:50)
                                    .foregroundColor(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                                    .padding(.horizontal,5)
                            }
                        }
                        .background(Color.white)
                        .padding(.leading, 10)
                        .padding(.trailing, 10)
                        .padding(.bottom, 10)
                        ScrollView(.vertical) {
                            VStack(alignment: .leading, spacing: 5) {
                                PromotionRow(lstPromociones: promos)
                                MenuCategorias(lstProducto: productos)
                                Text("Agregados Recientemente")
                                    .font(.title)
                                    .padding(.leading, 15)
                                    .padding(.top, 20)
                                ComponenteRow(lstComponentes: productos)
                                    .buttonStyle(PlainButtonStyle())
                            }
                        }
                        .onTapGesture {
                            self.isPressed.toggle()
                        }                    }
                }
                HStack {
                    Spacer()
                    if isPressed{
                        NavigationLink(destination:ContentView(components: productos)) {
                                Image("recomendacion")
                                    .resizable()
                                    .clipShape(Circle())
                                    .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 4))
                                    .frame(width:60,height: 60)
                                    .padding()
                                    .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
                .navigationBarTitle("Master")
                .onAppear(perform: cargar)
                .navigationBarItems(trailing: Button(action: {
                    self.isPerform.toggle()
                    if (self.defaults.string(forKey: "uid") != nil){
                        self.user = true
                    } else {
                        self.user = false
                    }
                }) {
                    if (defaults.string(forKey: "uid") == nil){
                        Image(systemName: "person")
                            .resizable()
                            .clipShape(Circle())
                            .foregroundColor(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                            .frame(width:45, height: 45)
                            .padding(.top,65)
                            .padding(.trailing,-3)
                    }else{
                        Image("william")
                            .resizable()
                            .clipShape(Circle())
                            .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 4))
                            .frame(width:45,height: 45)
                            .padding(.top,65)
                            .padding(.trailing,-3)
                    }
                }
                .buttonStyle(PlainButtonStyle())
                .sheet(isPresented: self.$isPerform) {
                    Group{
                        if self.user{
                            ProfileView().environmentObject(SessionStore())
                        }else{
                            loginView().environmentObject(SessionStore())
                        }
                    }
                })
            }
        }
    }
}
