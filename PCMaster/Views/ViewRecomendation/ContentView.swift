//
//  ContentView.swift
//  PCMaster
//
//  Created by Hugo Santiago on 28/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI
import Combine
import UIKit

extension Publishers {
    static var keyboardHeight: AnyPublisher<CGFloat, Never> {
        let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
            .map { $0.keyboardHeight }
        
        let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
            .map { _ in CGFloat(0) }
        
        return MergeMany(willShow, willHide)
            .eraseToAnyPublisher()
    }
}

extension Notification {
    var keyboardHeight: CGFloat {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
    }
}

struct ContentView: View {
    let recommendation = RecommendationController()
    
    var components :[Producto]
    
    @State private var texto: String = ""
    @State private var respuesta: String = ""
    @State private var animationAmount: CGFloat = 1
    @State private var keyboardHeight: CGFloat = 0

    var body: some View {
        VStack {
            Image("computer")
                .resizable()
                .clipShape(Circle())
                .frame(width: 200.0, height: 200.0)
                .overlay(Circle().stroke(Color.white,lineWidth:4).shadow(radius: 10))
                .overlay(
                    Circle()
                    .stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                    .scaleEffect(animationAmount)
                    .opacity(Double(2-animationAmount))
                        .animation(Animation.easeInOut(duration: 1.3)
                            .repeatForever(autoreverses: false))
            )
                .onAppear{
                    self.animationAmount = 2
            }
            
            Text("Cuentale a Master que necesitas")
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
                .font(.system(size: 30,weight: .bold))
                .padding()
            
            VStack{
                VStack (spacing: 20){
                    
                    Text(self.respuesta)
                        .frame(maxWidth: .infinity)
                        .font(.subheadline)
                        .padding()
                        .fixedSize(horizontal: false, vertical: true)
                        .background(Color(.sRGB, red: 233, green: 235, blue: 239))
                        .cornerRadius(15)
                        .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
                        .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
                        .padding()
                    VStack {
                        TextField("Que recomendación necesitas", text: $texto)
                            .font(.system(size:14))
                            .padding(12)
                            .background(RoundedRectangle(cornerRadius: 10).strokeBorder(Color(.gray), lineWidth:1 ))
                        
                        Button(action: {
                            self.recommendation.requestBot(texto: self.texto, callback: { (data:String) in
                                self.respuesta = data
                                if data.contains("especificaciones") {
                                    self.respuesta = data
                                    .replacingOccurrences(of: "\t", with: "")
                                }
                            } )
                            self.texto = ""
                        }) {
                            Text("Enviar")
                        }
                        .frame(minWidth:0, maxWidth: .infinity)
                        .frame(height:50)
                        .foregroundColor(.white)
                        .font(.system(size: 18, weight: .bold))
                        .background(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255))
                        .cornerRadius(5)
                        .padding()
                        
                    }
                    .frame(maxWidth: .infinity)
                    .font(.subheadline)
                    .padding()
                    .fixedSize(horizontal: false, vertical: true)
                    .background(Color(.sRGB, red: 233, green: 235, blue: 239))
                    .cornerRadius(15)
                    .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
                    .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
                    .padding()
                    
                    .padding(.bottom, keyboardHeight+45)
                    .onReceive(Publishers.keyboardHeight) { self.keyboardHeight = $0 }
                }
            }
        }
        
    }
}
