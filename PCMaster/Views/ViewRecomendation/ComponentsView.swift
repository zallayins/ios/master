//
//  ComponentsView.swift
//  PCMaster
//
//  Created by Hugo Santiago on 28/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct ComponentsView: View {
    var components:[Producto]
    
    var body: some View {
        ZStack(alignment: .bottom){
            ScrollView(){
                VStack{
                   ForEach(components){ compo in
                    NavigationLink(destination: ComponentDetail(component: compo).environmentObject(CarritoController())){
                                ComponentView(component: compo)
                                    .frame(width:350)
                                    .padding(.bottom, 10)
                        }
                    }
                }
            }
            HStack {
                Spacer()
                NavigationLink(destination:ARViewsita()) {
                    Image(systemName: "pencil.circle.fill")
                        .resizable()
                        .frame(width:80,height: 80)
                        .foregroundColor(Color(red: 153/255, green: 102/255, blue: 255/255))
                        .padding()
                }
            }
        }
    .navigationBarTitle(Text("Recomendacion"))
    }
}
