//
//  ComponentDetail.swift
//  PCMaster
//
//  Created by Hugo Santiago on 28/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI
import Combine

struct CardViewStore: View {
    @EnvironmentObject var carrito: CarritoController
    @State var alerta = false
    
    var producto:Producto
    var component:Producto.TiendaPrecio
    
    
    var body: some View {
        HStack{
            AsyncImage(url: URL(string: component.imagen_tineda ?? "https://images.vexels.com/media/users/3/157651/isolated/lists/f0fc37a5ee2d2ecf1ed147b528905394-icono-de-casa-tradicional.png")!, placeholder: Image(systemName: "goforward").frame(maxWidth: 350, maxHeight: 400))
                .cornerRadius(15)
                .frame(width: 60.0, height: 70.0)
            HStack {
                VStack(alignment: .leading, spacing: 0){
                    VStack (alignment: .trailing){
                        HStack {
                            Text("\(component.nombre_tienda ?? "Tauret")")
                                .foregroundColor(.primary)
                                .font(.system(size:15, weight: .medium))
                                .fontWeight(.black)
                            Text("$\(Int(component.precio))")
                            
                        }
                        HStack {
                            Text("\(component.nombre_tienda ?? "Tauret")")
                                .foregroundColor(.secondary)
                                .font(.caption)
                        }
                    }
                }
                Spacer()
                
                Button(action: {
                    self.carrito.addItem(self.component, producto: self.producto)
                    self.alerta = true
                }) {
                    Image(systemName: "cart.badge.plus")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 40)
                        .padding()
                }
                .alert(isPresented: $alerta) {
                    Alert(title: Text("Se ha añadido al carrito"))
                    
                }
            }
        }
        .background(Color(.sRGB, red: 233, green: 235, blue: 239))
        .cornerRadius(15)
        .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
        
    }
}

struct Specs: View {
    var component: Producto
    
    var body: some View {
        VStack {
            AsyncImage(url: URL(string: component.imagen)!, placeholder: Image(systemName: "goforward").frame(maxWidth: 350, maxHeight: 400))
                .cornerRadius(15)
            
            Text("\(component.descripcion ?? "No hay descripcion" )")
                .padding()
                .frame(maxWidth: 350, maxHeight: 400)
                .background(Color(.sRGB, red: 233, green: 235, blue: 239))
                .cornerRadius(20)
                .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
                .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
            
            Spacer()
        }
        
    }
}

struct ComponentDetail: View {
    @State var swtT: Bool = true
    @State var swtE: Bool = false
    
    var component:Producto
    var body: some View {
        VStack {
            if swtT == true { stores(component: component) }
            
            if swtE == true { Specs(component: component) }
            HStack{
                VStack {
                    Text("Tiendas")
                        .padding()
                        .onTapGesture {
                            if self.swtT == false { self.swtT = true }
                            if self.swtE == true { self.swtE = false }
                    }
                    if swtT {
                        Text("----")
                            .padding(.top, -20)
                    }
                }
                
                VStack {
                    Text("Especificaciones")
                        .padding()
                        .onTapGesture {
                            if self.swtE == false { self.swtE = true }
                            if self.swtT == true { self.swtT = false }
                    }
                    if swtE {
                        Text("---")
                            .padding(.top, -20)
                        
                    }
                }
            }
            .navigationBarTitle(Text(component.nombre), displayMode: .inline)
        }
    }
}
struct stores : View {
    var component: Producto
    var body: some View{
        
        Group{
            ZStack (alignment: .bottom) {
                AsyncImage(url: URL(string: component.imagen)!, placeholder: Image(systemName: "goforward"))
                    .cornerRadius(15)
                HStack{
                    Spacer()
                    NavigationLink(destination: ARViewsita()) {
                        Image("3d")
                            .resizable()
                            .frame(width: 35,height: 35)
                            .padding()
                    }
                }
            }
            .background(Color(.sRGB, red: 233, green: 235, blue: 239))
            .cornerRadius(15)
            .shadow(color: Color.black.opacity(0.2), radius: 7, x: 0, y: 2)
            
            ScrollView(.vertical){
                ForEach(component.tiendas, id : \.tienda ){store in
                    CardViewStore(producto: self.component, component: store)
                        .padding(.horizontal)
                }
            }
        }
    }
}


