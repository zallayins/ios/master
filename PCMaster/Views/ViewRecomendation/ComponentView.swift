//
//  Components.swift
//  PCMaster
//
//  Created by Hugo Santiago on 28/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

struct ComponentView: View {
    var component:Producto
    
    var body: some View {
        VStack (alignment: .leading, spacing: 16){
            Image(component.imagen)
                .resizable()
                .renderingMode(.original)
                .aspectRatio(contentMode: .fit)
                .cornerRadius(10)
            VStack(alignment: .leading, spacing: 5){
                Text("\(component.nombre)")
                    .foregroundColor(.primary)
                    .font(.title)
                    .fontWeight(.black)
                Text("Aprox \(component.nombre) dolares")
                    .foregroundColor(.secondary)
                    .font(.caption)
            }
        .padding()
            
        }
        .padding(.vertical)
        .background(Color(.sRGB, red: 233, green: 235, blue: 239))
        .cornerRadius(20)
        .shadow(color: Color.black.opacity(0.3), radius: 10, x: 10, y: 10)
        .shadow(color: Color.white.opacity(1), radius: 12, x: -8, y: -10)
    }
}
