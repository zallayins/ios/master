//
//  ViewCategoria.swift
//  PCMaster
//
//  Created by Hugo Santiago on 22/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI
import Combine

struct ViewCategoria: View {
    var productos:[Producto]
    var nameCategory:String
    
    var body: some View {
        VStack{
            List{
                ForEach(productos){item in
                    if(item.categoria == self.nameCategory){
                        NavigationLink(destination: ComponentDetail(component: item)){
                            viewRow(nombre: item.nombre,image: item.imagen)
                        }
                    }
                }
            }
        .navigationBarTitle(nameCategory)
        }
    }
}
struct viewRow:View {
    var nombre:String
    var image: String
    
    var body:some View{
        HStack{
            AsyncImage(url: URL(string: self.image)!, placeholder: Image(systemName: "goforward"))
                .frame(width:70, height:70)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color(.sRGB, red: 68/255, green: 77/255, blue: 255/255), lineWidth: 4))
            Text(nombre)
        }
    }
}
