//
//  ProductController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 18/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class ProductsController {
    var productColletion: ProductosCollection
    
    init() {
        productColletion = ProductosCollection()
    }
    
    func getProductsName(callback: @escaping ([Producto]) -> Void){
        callback(productColletion.lstEntity)
    }
    
}
