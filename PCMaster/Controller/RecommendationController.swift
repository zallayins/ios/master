//
//  ContectLogic.swift
//  PCMaster
//
//  Created by Hugo Santiago on 18/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation
import ApiAI
import AVFoundation


class RecommendationController {
    init(){}
    
    func requestBot(texto: String, callback: @escaping (String) -> Void){
        let request = ApiAI.shared().textRequest()
        
        var respuesta:String.SubSequence = ""
        
        if texto != "" {
            request?.query = texto
        }
        ApiAI.shared().enqueue(request)
        
        request?.setMappedCompletionBlockSuccess({ (request, response) in
            
            let response = response as! AIResponse
            let textResponse = response.result.fulfillment.speech
            
            if (textResponse == ""){
                respuesta = "No hay respuesta del servidor. Por favor intente más tarde"
                callback(String(respuesta))
            } else {
                respuesta = textResponse?.split(separator: "\"")[0] ?? "Ha occurido un error, intente más tarde"
                callback(String(respuesta))
            }
        }, failure: { (request, error) in
            print(error!)
        })
    }
}
