//
//  UserController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 26/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//
import Foundation
import Firebase

class UserController: ObservableObject {
    var compritas = [compras]()
    
    @Published var user:usuario = usuario()
    
    
    func AnyToItem(elAny: [Any]) -> [items]{
        var aux = [items]()
        for cosito in elAny {
            let cantidad = (cosito as! [String:Any])["cantidad"]! as! Int
            let imagen = (cosito as! [String:Any])["imagen"]! as! String
            let precio = (cosito as! [String:Any])["precio"]! as! Double
            let productName = (cosito as! [String:Any])["producto"]! as! String
            let tiendaName = (cosito as! [String:Any])["tienda"]! as! String
            aux.append(items(cantidad: cantidad, imagen: imagen, precio: precio, productName: productName, tiendaName: tiendaName))
        }
        return aux
    }
    
    
    init(idUser: String) {
        let db = Firestore.firestore()
        
        db.collection("usuarios").whereField("id", isEqualTo: idUser).addSnapshotListener { (snap, err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            for i in snap!.documentChanges{
                let id = i.document.documentID
                let correo = i.document.get("correo") as! String
                let celular = i.document.get("celular") as! String
                let direccion = i.document.get("direccion") as! String
                let nombre = i.document.get("nombre") as! String
                
                db.collection("usuarios").document(idUser).collection("compras").addSnapshotListener { (snap, err) in
                    if err != nil{
                        print((err?.localizedDescription)!)
                    }
                    for i in snap!.documentChanges{
                        let id = i.document.documentID
                        let fecha = i.document.get("fecha") as! Timestamp
                        let total = i.document.get("total") as! Double
                        let itemsC = i.document.get("items").unsafelyUnwrapped as! [Any]
                        let resItem = self.AnyToItem(elAny: itemsC)
                        self.compritas.append(compras(id: id, fecha: fecha, total: total,itemsitos: resItem))
                    }
                    self.user = usuario(id: id, celular: celular, correo: correo, direccion: direccion, nombre: nombre, compras: self.compritas )
                }
            }
        }
    }
}


struct usuario: Identifiable {
    var id: String = ""
    var celular: String = ""
    var correo: String = ""
    var direccion: String = ""
    var nombre: String = ""
    var compras: [compras] = []
}

struct compras: Identifiable{
    var id: String
    var fecha: Timestamp
    var total: Double
//    var items: [String:Any?]
    var itemsitos: [items]
    
}

struct items: Identifiable {
    var id = UUID()
    var cantidad: Int
    var imagen: String
    var precio: Double
    var productName: String
    var tiendaName: String
    
}

