//
//  CategController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 18/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class CategController {
    var categColletion: CategoriaCollection
    
    init() {
        categColletion = CategoriaCollection()
    }
    
    func getCategsName(callback: @escaping ([Categoria]) -> Void){
        callback(categColletion.lstEntity)
    }
}
