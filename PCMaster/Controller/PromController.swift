//
//  PromController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 18/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class PromController {
    var promColletion: PromocionesCollection
    
    init() {
        promColletion = PromocionesCollection()
    }
    
    func getPromsName(callback: @escaping ([Promocion]) -> Void){
        callback(promColletion.lstEntity)
    }
}
