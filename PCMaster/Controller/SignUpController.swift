//
//  SignUpController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 26/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//
import Foundation
import Combine
import Navajo_Swift

class SignUpController: ObservableObject{
    // Input
    @Published var user = ""
    @Published var password = ""
    @Published var passAgain = ""
    
    //output
    
    @Published var usernameMessage = ""
    @Published var passwordMessage = ""
    @Published var isValid = false

    
    private var cancellableSet: Set<AnyCancellable> = []
    
    private var isPasswordEmptyPublisher: AnyPublisher<Bool, Never> {
      // (1)
      $password
        .debounce(for: 0.8, scheduler: RunLoop.main)
        .removeDuplicates()
        .map { password in
          return password == ""
        }
        .eraseToAnyPublisher()
    }
    
    private var arePasswordsEqualPublisher: AnyPublisher<Bool, Never> {
      Publishers.CombineLatest($password, $passAgain)
        .debounce(for: 0.2, scheduler: RunLoop.main)
        .map { password, passAgain in
          return password == passAgain
        }
        .eraseToAnyPublisher()
    }
    
    private var passwordStrengthPublisher: AnyPublisher<PasswordStrength, Never> {
      $password
        .debounce(for: 0.2, scheduler: RunLoop.main)
        .removeDuplicates()
        .map { input in
          return Navajo.strength(ofPassword: input) // (2)
        }
        .eraseToAnyPublisher()
    }
    
    private var isPasswordStrongEnoughPublisher: AnyPublisher<Bool, Never> {
      passwordStrengthPublisher
        .map { strength in
          switch strength {
          case .reasonable, .strong, .veryStrong:
            return true
          default:
            return false
          }
        }
        .eraseToAnyPublisher()
    }
    
    enum PasswordCheck {
      case valid
      case empty
      case noMatch
      case notStrongEnough
    }
    
    private var isPasswordValidPublisher: AnyPublisher<PasswordCheck, Never> {
      Publishers.CombineLatest3(isPasswordEmptyPublisher, arePasswordsEqualPublisher, isPasswordStrongEnoughPublisher)
        .map { passwordIsEmpty, passwordsAreEqual, passwordIsStrongEnough in
          if (passwordIsEmpty) {
            return .empty
          }
          else if (!passwordsAreEqual) {
            return .noMatch
          }
          else if (!passwordIsStrongEnough) {
            return .notStrongEnough
          }
          else {
            return .valid
          }
        }
        .eraseToAnyPublisher()
    }
    
    private var isUsernameValidPublisher: AnyPublisher<Bool, Never> {
      $user
        .debounce(for: 0.8, scheduler: RunLoop.main)
        .removeDuplicates()
        .map { input in
          return input.count >= 3
        }
        .eraseToAnyPublisher()
    }
    
    private var isFormValidPublisher: AnyPublisher<Bool, Never> {
      Publishers.CombineLatest(isUsernameValidPublisher, isPasswordValidPublisher)
        .map { usernameIsValid, passwordIsValid in
          return usernameIsValid && (passwordIsValid == .valid)
        }
      .eraseToAnyPublisher()
    }
    
    init() {
      isUsernameValidPublisher
        .receive(on: RunLoop.main)
        .map { valid in
          valid ? "" : "El correo debe tener más de 3 caracteres"
        }
        .assign(to: \.usernameMessage, on: self)
        .store(in: &cancellableSet)
      
      isPasswordValidPublisher
        .receive(on: RunLoop.main)
        .map { passwordCheck in
          switch passwordCheck {
          case .empty:
            return "Debes poner una contraseña"
          case .noMatch:
            return "Los campos no son iguales"
          case .notStrongEnough:
            return "La contraseña no es lo suficientemente fuerte"
          default:
            return ""
          }
        }
        .assign(to: \.passwordMessage, on: self)
        .store(in: &cancellableSet)

      isFormValidPublisher
        .receive(on: RunLoop.main)
        .assign(to: \.isValid, on: self)
        .store(in: &cancellableSet)
    }
}
