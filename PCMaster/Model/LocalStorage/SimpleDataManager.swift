//
//  SimpleDataManager.swift
//  PCMaster
//
//  Created by William Duarte on 18/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

struct SimpleDataManager {
    
    static private let defaults = UserDefaults.standard;
    
    static func existsKey(key:String) -> Bool {
        if defaults.object(forKey: key) != nil {
            return true;
        } else {
            return false;
        }
    }
    
    static func set(key:String, value:Any) {
        defaults.set(value, forKey: key);
        defaults.synchronize()
    }
    
    static func setEntity<Entity:BaseEntity>(key:String, entity:Entity) {
        defaults.set(entity.dictionary, forKey: key);
        defaults.synchronize()
    }
    
    static func setLstEntity<Entity:BaseEntity>(key:String, lstEntity:[Entity]) {
        defaults.set(lstEntity.map{ e in e.dictionary } , forKey: key);
        defaults.synchronize()
    }
    
    static func getInt(key:String) -> Int? {
        if existsKey(key: key) {
            return defaults.integer(forKey: key);
        } else {
            return nil;
        }
    }
    
    static func getString(key:String) -> String? {
        if existsKey(key: key) {
            return defaults.string(forKey: key);
        } else {
            return nil;
        }
    }
    
    static func getDictionary(key:String) -> [String:Any] {
        if existsKey(key: key) {
            return defaults.dictionary(forKey: key)!;
        } else {
            return [:];
        }
    }
    
    static func getEntity<Entity:BaseEntity>(key:String) -> Entity? {
        if existsKey(key: key) {
            let info = defaults.dictionary(forKey: key)!;
            let jsonData = (try? JSONSerialization.data(withJSONObject: info, options: []))!;
            return (try? JSON.decoder.decode( Entity?.self, from: jsonData));
        } else {
            return nil;
        }
    }
    
    static func getLstEntities<Entity:BaseEntity>(key:String, type:Entity?.Type) -> [Entity?]? {
        if existsKey(key: key) {
            let info = defaults.array(forKey: key)!;
            return info.map {e in
                let jsonData = (try? JSONSerialization.data(withJSONObject: e, options: []))!;
                return (try? JSON.decoder.decode( type, from: jsonData));
            }.filter { e in e != nil }
        } else {
            return nil;
        }
    }
    
    static func removeElement(key: String) {
        if existsKey(key: key) {
            defaults.removeObject(forKey: key)
        }
    }
    
}
