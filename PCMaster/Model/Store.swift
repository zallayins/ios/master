//
//  Store.swift
//  PCMaster
//
//  Created by William Duarte on 27/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation


/**
 Estructura que representa las tiendas
 */
struct Store: Hashable, Codable, Identifiable {
    var id = UUID()
    var name: String
    var address:String
    var precio: Int
}
