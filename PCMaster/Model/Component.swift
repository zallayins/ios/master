//
//  ComponentsData.swift
//  PCMaster
//
//  Created by Hugo Santiago on 28/02/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import SwiftUI

/**
 Estructuta que representa la informaciòn de los componentes y partes de computadoras
 */
struct Component: Hashable, Codable ,Identifiable {
    var id = UUID()
    var name:String
    var precio:Int
    var image:String
    var description:String
    var categoryName:Categorias
    var store:[Store]
}

let des:String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at cursus magna. Curabitur suscipit finibus ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer lobortis vitae elit ac tristique. Phasellus placerat consectetur est rhoncus hendrerit. Duis bibendum purus dapibus iaculis placerat."

let store1 = Store(name:"Turet",address:"Unilago 201",precio:150000)
let store2 = Store(name:"Amazon",address:"Amazon.com",precio:130000)
let store3 = Store(name:"Cometware",address:"Unilago 144",precio:140000)
let store4 = Store(name:"Wish",address:"Wish.com",precio:180000)
let store5 = Store(name:"BestBuy",address:"bestbuy.com",precio:150000)

let test:[Component] = [
    Component(name:"Nvidia 1080 TI", precio:500, image:"graphics", description: des, categoryName: Categorias.CATEGORIA1, store: [store1,store2,store4,store5,store3]),
    Component(name:"intel Core i9 9900k",precio:300, image:"cpu",description: des, categoryName: Categorias.CATEGORIA1, store:[store3,store2,store4,store1,store5]),
    Component(name:"Asus Xtreme x550",precio: 150,image:"motherboard",description: des, categoryName: Categorias.CATEGORIA1, store:[store1,store2,store4]),
    Component(name:"16GB ram 3000mhz",precio: 50,image:"ram",description: des, categoryName: Categorias.CATEGORIA1, store:[store1,store5,store2]),
    Component(name:"Samsung M.2 EVO 960GB",precio: 150,image:"ssd",description: des, categoryName: Categorias.CATEGORIA1, store:[store2,store3]),
    Component(name:"Benq Monitor 240Hz 1ms",precio: 400,image:"monitor",description: des, categoryName: Categorias.CATEGORIA2, store:[store3,store4,store5]),
    Component(name:"MK logitech",precio: 150,image:"keyboard",description: des, categoryName: Categorias.CATEGORIA2, store:[store2,store3,store4]),
    Component(name:"G pro wireless",precio: 100, image:"mouse",description: des, categoryName: Categorias.CATEGORIA2, store:[store1,store2,store3,store4]),
    Component(name:"BlackPanter elite case",precio: 200,image:"case",description: des, categoryName: Categorias.CATEGORIA3, store:[store1,store4]),
    Component(name:"Power 1200W 80 plus",precio: 120,image:"power",description: des, categoryName: Categorias.CATEGORIA3, store:[store1,store3,store5]),
]

