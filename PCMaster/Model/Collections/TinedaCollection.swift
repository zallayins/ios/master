//
//  TinedaCollection.swift
//  PCMaster
//
//  Created by William Duarte on 9/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class  TiendaCollection: FirebaseDBLib<Tienda> {
    init() {
        super.init(collectionName: "tienda");
    }
}
