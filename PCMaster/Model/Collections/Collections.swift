//
//  Collections.swift
//  PCMaster
//
//  Created by William Duarte on 9/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

struct Collections {
    public static let categorias = CategoriaCollection();
    public static let facturas = FacturaCollection();
    public static let productos = ProductosCollection();
    public static let tiendas = TiendaCollection();
    public static let usuarios = UsuariosCollection();
    public static let promociones = PromocionesCollection();
}
