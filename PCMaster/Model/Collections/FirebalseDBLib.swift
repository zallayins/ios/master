//
//  FirebalseDBLib.swift
//  PCMaster
//
//  Created by William Duarte on 4/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation
import Firebase

class FirebaseDBLib <Entity:BaseEntity> : ObservableObject {
    //------------------------
    // Atributos
    //------------------------
    var collectionName:String
    var db:Firestore;
    var collection:CollectionReference;
    
    @Published var lstEntity:[Entity];
    
    //------------------------
    // Constructor
    //------------------------
    init(collectionName:String) {
        self.collectionName = collectionName;
        db = Firestore.firestore();
        collection = db.collection(collectionName);
        lstEntity = [Entity]();
        getAllDB { (lst, error) in
            if error != nil {
                print("Error al realizar el cargue inicial de la entidad \(collectionName)")
                print((error?.localizedDescription)!)
            }
        }
    }
    
    //------------------------
    // Métodos
    //------------------------
    func add(entity:Entity) {
        var dicData = entity.dictionary;
        dicData["id"] = collection.document().documentID;
        collection.addDocument(data: entity.dictionary) { error in
            if error != nil{
                print("Error agregando en la entidad \(self.collectionName)")
                print((error?.localizedDescription)!)
            } else {
                self.lstEntity.append(entity);
            }
        }
    }
    
    func getDB(id:String, callback:@escaping ((Entity?, Error?) -> ())) {
        collection.document(id).getDocument { (document, error) in
            var data:Entity? = nil;
            if error == nil {
                data = self.decode(dicValue: document!.data()!)
                callback(data, nil);
            } else {
                callback(nil, error);
            }
        }
    }
    
    func get(id:String) -> Entity? {
        for e in lstEntity {
            if((e.id as? String)! == id) {
                return e;
            }
        }
        return nil;
    }
    
    func getAllDB(callback:@escaping (([Entity], Error?) -> ())) {
        collection.getDocuments { (snapshot, error) in
            var lst = [Entity]();
            if error == nil && snapshot != nil {
                for document in snapshot!.documents {
                    var dicEntityValues = document.data();
                    dicEntityValues["id"] = document.documentID;
                    let entity = self.decode(dicValue: dicEntityValues);
                    if(entity != nil) {
                        lst.append(entity!);
                    }
                }
                self.lstEntity = lst;
                callback(lst, nil);
            } else {
                callback([Entity](), error);
            }

        }
    }
    
    func getAll() -> [Entity] {
        return lstEntity;
    }
    
    func update(id:String, entity:Entity) {
        if let e = get(id: id) {
            var dic = entity.dictionary;
            dic.merge(e.dictionary) { (a,_) in a }
            dic["id"] = id;
            collection.document(id).setData(dic, merge: true) { (error) in
                if error != nil {
                    self.lstEntity = self.lstEntity.map { e in
                        if ((e.id as? String)! == id)
                        {
                            return self.decode(dicValue: dic)!;
                        } else {
                            return e;
                        }
                    }
                }
            }
        }
    }
    
    //------------------------
    // Métodos auxiliares
    //------------------------
    private func decode(dicValue:[String:Any?]) -> Entity? {
        let jsonData = (try? JSONSerialization.data(withJSONObject: dicValue, options: []))!;
        return (try? JSON.decoder.decode( Entity?.self, from: jsonData));
    }
}
