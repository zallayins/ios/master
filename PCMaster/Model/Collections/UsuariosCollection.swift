//
//  UsuariosCollection.swift
//  PCMaster
//
//  Created by William Duarte on 9/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class  UsuariosCollection: FirebaseDBLib<Usuario> {
    init() {
        super.init(collectionName: "usuarios");
    }
}
