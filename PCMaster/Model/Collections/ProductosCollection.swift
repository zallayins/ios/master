//
//  ProductosCollections.swift
//  PCMaster
//
//  Created by William Duarte on 9/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

class  ProductosCollection: FirebaseDBLib<Producto> {
    init() {
        super.init(collectionName: "productos");
    }
}
