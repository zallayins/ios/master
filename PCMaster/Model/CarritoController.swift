//
//  CarritoController.swift
//  PCMaster
//
//  Created by Hugo Santiago on 23/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

 

class CarritoController: ObservableObject {
    
    var didChange = PassthroughSubject<CarritoController, Never>()
    @Published var carrito: [CarritoItemcito] { didSet {self.didChange.send(self)} }
    
    init() {
        carrito = []
        if SimpleDataManager.existsKey(key: "carrito") {
            let carritoTemp = SimpleDataManager.getLstEntities(key: "carrito", type: CarritoItemcito?.self);
            for e in carritoTemp! {
                if e != nil {
                    carrito.append(e!);
                }
            }
        }
    }
    
    func addItem(_ item: Producto.TiendaPrecio, producto:Producto){
        var encontro:Bool = false;
        for (i, e) in carrito.enumerated() {
            if e.tiendaID == item.tienda && e.productoID == producto.id {
                carrito[i].cantidad += 1;
                encontro = true;
                break;
            }
        }
        if !encontro {
            carrito.append(CarritoItemcito(
                imgProducto: producto.imagen,
                productoID: producto.id,
                productoNombre: producto.nombre,
                tiendaID: item.tienda,
                tiendaNombre: item.nombre_tienda ?? "TAURET SAS",
                tiendaImagen: item.imagen_tineda ?? "https://icons.iconarchive.com/icons/paomedia/small-n-flat/256/shop-icon.png",
                productoImagen: producto.imagen,
                precio: item.precio,
                cantidad:1
            ))
        }
        
        SimpleDataManager.setLstEntity(key: "carrito", lstEntity: carrito)
        print(carrito.count.description)
    }
    
    func deleteItem(_ itemsito: CarritoItemcito){
        for (i, e) in carrito.enumerated() {
            if e.id == itemsito.id {
                carrito.remove(at: i)
                break;
            }
        }
    }
    func pagar(){
        carrito = []
        SimpleDataManager.removeElement(key: "carrito")
    }
    
    func _calcular() -> Double {
        var result:Double = 0
        for item in carrito{ result += item.precio*Double(item.cantidad) }
        return result
    }
    
    func calcularTotal() -> Int{
        return Int(_calcular())
    }
    
    func calcularTotalIVA() -> Int{
        return Int(_calcular()*0.19)
    }
    
}

struct CarritoItemcito: BaseEntity {
    var id = UUID()
    var imgProducto: String
    var productoID:String
    var productoNombre: String
    var tiendaID: String
    var tiendaNombre: String
    var tiendaImagen: String
    var productoImagen: String
    var precio: Double
    var cantidad:Int
}
