//
//  Category.swift
//  PCMaster
//
//  Created by William Duarte on 26/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

struct Category : Hashable, Codable ,Identifiable {
    var id = UUID()
    var name:String
    var sysImage:String?
    var assetImage:String?
    var colorHex:String
}

enum Categorias: String, CaseIterable, Codable, Hashable, Comparable {
    static func < (lhs: Categorias, rhs: Categorias) -> Bool {
        return lhs.rawValue < rhs.rawValue;
    }
    
    case CATEGORIA1 = "Tarjeta gráfica"
    case CATEGORIA2 = "Memoria RAM"
    case CATEGORIA3 = "SSD"
    case CATEGORIA4 = "Monitor"
    case CATEGORIA5 = "Teclado"
    case CATEGORIA6 = "Procesador"
    case CATEGORIA7 = "Mouse"
    case CATEGORIA8 = "Fuente de alimentación"
    case CATEGORIA9 = "Gabinete"
}

let lstCategoriasTest:[Category] = [
    Category(name: Categorias.CATEGORIA1.rawValue, sysImage: "gpu", colorHex: "FF0040"),
    Category(name: Categorias.CATEGORIA2.rawValue, sysImage: "ram", colorHex: "FF8000"),
    Category(name: Categorias.CATEGORIA3.rawValue, sysImage: "ssd-drive", colorHex: "FF8000"),
    Category(name: Categorias.CATEGORIA4.rawValue, sysImage: "computer-1", colorHex: "FF8000"),
    Category(name: Categorias.CATEGORIA5.rawValue, sysImage: "keyboard-1", colorHex: "FF0040"),
    Category(name: Categorias.CATEGORIA6.rawValue, sysImage: "cpu", colorHex: "00FF00"),
    Category(name: Categorias.CATEGORIA7.rawValue, sysImage: "mouse", colorHex: "2E2EFE"),
    Category(name: Categorias.CATEGORIA8.rawValue, sysImage: "supply", colorHex: "00FF00"),
    Category(name: Categorias.CATEGORIA9.rawValue, sysImage: "case", colorHex: "2E2EFE"),
]

