//
//  Promotions.swift
//  PCMaster
//
//  Created by William Duarte on 26/03/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation
import SwiftUI

struct Promotion: Hashable, Codable ,Identifiable {
    var id = UUID()
    var tienda:Store
    var producto:Component
    var precio:Int
    var descuento:Double
    var description:String?
}

let lstPromotionTest:[Promotion] = [
    Promotion(tienda: store1, producto: test[0], precio: 10000, descuento: 0.1),
    Promotion(tienda: store1, producto: test[1], precio: 10000, descuento: 0.2),
    Promotion(tienda: store2, producto: test[2], precio: 10000, descuento: 0.05),
    Promotion(tienda: store2, producto: test[3], precio: 10000, descuento: 0, description: "2x1"),
    Promotion(tienda: store4, producto: test[4], precio: 10000, descuento: 0.9, description: "Llevate la segunda unidad a un 90% de descuento"),
    Promotion(tienda: store5, producto: test[4], precio: 10000, descuento: 0.9, description: des)
]
