//
//  Categorias.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

struct Categoria:BaseEntity {
    //------------------------
    // Atributos
    //------------------------
    var id: String;
    var nombre:String;
}

//------------------------
// Método ToString
//------------------------
extension Categoria {
    public var description:String { return nombre; }
}
