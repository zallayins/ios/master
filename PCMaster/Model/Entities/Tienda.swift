//
//  Tienda.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

struct Tienda:BaseEntity {
    var id:String
    var imagen:String
    var nombre:String
    var url:String
}
