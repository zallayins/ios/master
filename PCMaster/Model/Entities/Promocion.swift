//
//  Promociones.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

struct Promocion:BaseEntity {
    var id:String
    var precio:Double
    var descuento:Double
    var descripcion:String
    var imagen:String
    var numeral:String
    var tienda:String
    var producto:String
    
}
