//
//  Factura.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

struct Factura:BaseEntity {
    var id:String
    var fecha:Date
    var total:Double
    var entrega:Entrega?
    
    var usuario:String
    var descripcion:[Detalle]?
    
    private var _usuario_entity:Usuario?;
    var usuarioE:Usuario? {
        mutating get {
            if _usuario_entity == nil {
                _usuario_entity = Collections.usuarios.get(id: usuario);
            }
            return _usuario_entity;
        }
    }
    
}

extension Factura {
    func getDateToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_CO")
        dateFormatter.dateFormat = "dd-MM-yyyy' 'hh:mm:ss"
        return dateFormatter.string(from: self.fecha)
    }
}

extension Factura {
    struct Entrega:Codable {
        var direccion:String
        var costo:Double
        var fecha_entrega:Date
    }
    struct Detalle:Codable {
        var cantidad:Int
        var subtotal:Double
        var tienda:String
        var producto:String
        
        private var _tienda_entity:Tienda?;
        var tiendaE:Tienda? {
            mutating get {
                if _tienda_entity == nil {
                    _tienda_entity = Collections.tiendas.get(id: tienda);
                }
                return _tienda_entity;
            }
        }
        
        private var _producto_entity:Producto?;
        var productoE:Producto? {
            mutating get {
                if _producto_entity == nil {
                    _producto_entity = Collections.productos.get(id: producto);
                }
                return _producto_entity;
            }
        }
    }
}
