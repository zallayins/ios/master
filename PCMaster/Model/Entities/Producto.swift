//
//  Producto.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

struct Producto:BaseEntity {
    var id:String
    var nombre:String
    var descripcion:String?
    var imagen:String
    var categoria:String
    var tiendas:[TiendaPrecio]

}
extension Producto {
    struct TiendaPrecio: Codable{
        
        var tienda:String
        var precio:Double
        var nombre_tienda:String?
        var imagen_tineda:String?
        var numeral_tienda:String?
        
        private var _tienda_entity:Tienda?;
        var tiendaE:Tienda? {
            mutating get {
                if _tienda_entity == nil {
                    _tienda_entity = Collections.tiendas.get(id: tienda);
                }
                return _tienda_entity;
            }
        }
    }
}


