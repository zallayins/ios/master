//
//  BaseEntity.swift
//  PCMaster
//
//  Created by William Duarte on 8/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//

import Foundation

protocol BaseEntity: Codable, Identifiable {
}

struct JSON {
    static let encoder = JSONEncoder()
    static let decoder = JSONDecoder()
    
    
    static func decode<Entity:Codable>(dicValue: String) -> Entity? {
        let jsonData = (try? JSONSerialization.data(withJSONObject: dicValue, options: []))!;
        return (try? JSON.decoder.decode( Entity?.self, from: jsonData));
    }
    
    
}

extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
