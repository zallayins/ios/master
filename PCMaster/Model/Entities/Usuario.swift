//
//  Usuario.swift
//  PCMaster
//
//  Created by William Duarte on 3/04/20.
//  Copyright © 2020 Hugo Santiago. All rights reserved.
//
import Foundation

struct Usuario:BaseEntity {
    var id:String
    var nombre:String
    var correo:String
    var direccion:String
    var imagen:String
    var medio_pago:[MedioPago]?
    var reserva:[Reserva]?
    var hitorial_facturas:[String];
    
    private var _hitorial_facturas_entity:[Factura]?;
    var hitorial_facturasE:[Factura]? {
        mutating get {
            if _hitorial_facturas_entity == nil {
                _hitorial_facturas_entity = [Factura]();
                for factura_id in hitorial_facturas {
                    _hitorial_facturas_entity?.append(Collections.facturas.get(id: factura_id)!);
                }
            }
            return _hitorial_facturas_entity;
        }
    }
}

extension Usuario {
    struct MedioPago:Codable {
        var descripcion:String
        var numero_cuenta:String
    }
    struct Reserva:Codable {
        var tienda:String
        var producto:String
        var fecha_reserva:Date?
        var cantidad:Int
        
        private var _tienda_entity:Tienda?;
        var tiendaE:Tienda? {
            mutating get {
                if _tienda_entity == nil {
                    _tienda_entity = Collections.tiendas.get(id: tienda);
                }
                return _tienda_entity;
            }
        }
        
        private var _producto_entity:Producto?;
        var productoE:Producto? {
            mutating get {
                if _producto_entity == nil {
                    _producto_entity = Collections.productos.get(id: producto);
                }
                return _producto_entity;
            }
        }
    }
}
